# Introduction

The `master` and `devel` branches of this repository contains the scripts that generate the sklearn wrapper code using Jinja2 templates. The wrapper code follows the [D3M TA 1 Python interface](.https://gitlab.com/datadrivendiscovery/d3m#python-interfaces-for-ta1-primitives).

Generated primitives are build automatically and pushed to [`dist`](https://gitlab.com/datadrivendiscovery/sklearn-wrap/tree/dist) and [`dev-dist`](https://gitlab.com/datadrivendiscovery/sklearn-wrap/tree/dev-dist) branches of this repository.

## Installation

To install dependencies for generator scripts, using pip 19+:

```
pip install -r requirements.txt
```

To install stable version of generated primitives from repository:

```
pip install -e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dist#egg=sklearn_wrap
```

Or version from `devel` branch development version:

```
pip install -e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dev-dist#egg=sklearn_wrap
```

## Generation

The auto generation in broken down into two steps.

### Curation: 

We need to augment the sklearn base classes with some additional metadata about the hyperparameters. 
This additional metadata is captured in an overlay file. You can find the overlay files in the tests/resources directory. 

To start curating a new sklearn class, you need to first generate the template. 
Run 

```
python -m autogenerate.generate_overlay_template <sklearn class name> <overlay file>
```

This will create a new parsed json in the directory - [`autogenerate/resources/primitive-jsons`](./autogenerate/resources/primitive-jsons) and create a blank template in the overlay file for the user to curate. 


### Generation:

Now to generate the wrapped code, run [`autogenerate/wrap_sklearn.py`](./autogenerate/wrap_sklearn.py).


```
python3 -m autogenerate.wrap_sklearn --dir autogenerate/resources/primitive-jsons --overlays autogenerate/resources/overlay_extras.json autogenerate/resources/overlay_classifiers.json autogenerate/resources/overlay_regressors.json autogenerate/resources/overlay_preprocessors.json autogenerate/resources/overlay_semi_supervised.json --module_config autogenerate/config/setup.config.json --primitive_id_file autogenerate/d3m_sklearn_wrap_ids.json --hyperparams_to_tune autogenerate/resources/hyperparams_to_tune.yml
```

- This script should create a directory `d3m_sklearn_wrap` with the `setup.py` file in the `autogenerate` directory.

- It will also generate individual test cases for each class and place it in the `tests` directory. 

If the user does not want to generate the entire `d3m_sklearn_wrap` module directory but only generate the code for
the classes to test, they can run the [`autogenerate/autogen_supervised.py`](autogenerate/autogen_supervised.py) script
which will generate the output directory with the classes which are present in the overlay file.


## Tests

You can run generated tests by running `run_tests.sh`.

Note: To run the tests, it is assumed that the user has the `d3m_skearn_wrap` directory within the `autogenerate` dir. 


## Releasing new versions

1.) Work within the most up to date docker image `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-artful-python36-<version>` More information on docker images [here](https://gitlab.com/datadrivendiscovery/images).

2.) Clone this repository's `devel` branch `git clone https://gitlab.com/datadrivendiscovery/sklearn-wrap`

3.) Generate all of the SKLearn primitives and their tests from the root of the repository: 
```
python3 -m autogenerate.wrap_sklearn --dir autogenerate/resources/primitive-jsons --overlays autogenerate/resources/overlay_extras.json autogenerate/resources/overlay_classifiers.json autogenerate/resources/overlay_regressors.json autogenerate/resources/overlay_preprocessors.json --module_config autogenerate/config/setup.config.json --primitive_id_file autogenerate/d3m_sklearn_wrap_ids.json --hyperparams_to_tune autogenerate/resources/hyperparams_to_tune.yml
```
3a.) If you want to run the tests and `common primitives` is not in the docker image, then you will have to clone the common primitives repo, `pip3 install -e .` from root of that repository.
3b.) The tests run on the CI automatically on new pushes to SKLearn repo, but if you want to run locally, you can execute via: `./run_tests.sh` in the root of this repository.
3c.) The CI then goes and auto generates the python pipelines and then execute all of those to the json versions, if you want to see them run locally, you can do:
```
cd autogenerate
python3 generate_primitive_jsons.py
```

Running this will create a new directory called <version> which will contain all of the primitive.json and pipeline json files (<pipeline_id>.json) for each primitive under its namespace directory. 

*NOTE: You cannot copy these directory over to common primitives because they will contain the wrong commit hash that is getting dynamically inserted. You need to finish the rest of this process to get a proper run through of primitives repo CI.

4.) `git add <version>`

5.) `git commit`

6.) `git push`

At this point the CI will run through all of the tests that were autogenerated as well as create the example json pipelines.

If the CI passes, make a merge into `master` from `devel` in git, which will run the same CI process and will then expose a downloadable `artifacts` download.

7.) Download the artifacts file from the CI and unzip it. It is a copy of the `<version>` directory containing all of the primitives.

7.) Fork the [primitives repo](https://gitlab.datadrivendiscovery.org/jpl/primitives_repo). Name the branch '<team>_<primitive>'

8.) If you `cd` into the `<version_number>` directory from the downloaded artifact, then you can copy the output into your forked primitives repo like so:
```
cp -r JPL /<path_to_primitives_repo>/primitives_repo/v2019.1.21
```

9.) Switch over to your fork of the common primitives repo and git add / commit / push. Then make the merge request into the main common primitives repository.