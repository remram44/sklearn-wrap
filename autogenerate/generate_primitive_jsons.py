import argparse
import os, sys, pip, time, json
from d3m import index
from autogenerate.pipeline_example import pipeline_structure
current_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), os.pardir))
from pathlib import Path
import shutil


def loop_through():
    all_primitives = index.search()
    for primitive in all_primitives:
        if "SKlearn" in primitive:
            primitive_obj = index.get_primitive(primitive)
            print(primitive)
            if "mutual_info" in primitive:
                continue
            primitive_json = primitive_obj.metadata.to_json_structure()
            author = primitive_json.get('source').get('name')
            interface_version = primitive_json.get('primitive_code').get('interfaces_version')
            version = primitive_json.get('version')
            primitive_json_path = "{interface_version}/{author}/{primitive_path}/{version}".\
                format(interface_version=interface_version, author=author, primitive_path=primitive, version=version)

            output_path = os.path.join(current_dir, primitive_json_path)
            """
            if os.path.exists(output_path):
                os.rmdir(output_path)
            os.makedirs(output_path)
            """
            p = Path(output_path)
            try:
                if p.exists():
                    shutil.rmtree(p)
            except Exception as e:
                print("Error removing trees")
                print(e)
            Path(output_path).mkdir(exist_ok=True,parents=True)
            Path(output_path + "/pipelines").mkdir(exist_ok=True, parents=True)



            generate_primitive_json(current_dir, primitive_json, primitive_json_path)
            generate_pipeline_json(current_dir, primitive, primitive_json_path)


def generate_primitive_json(current_dir, primitive_json, primitive_json_path):

    with open(os.path.join(current_dir, primitive_json_path, "primitive.json"), 'w') as fw:
        fw.write(json.dumps(primitive_json, indent=2))

    return


def generate_pipeline_json(current_dir, primitive, primitive_json_path):

    pipeline_dict, metadata_json = pipeline_structure(primitive)
    json_id = pipeline_dict["id"]

    with open(os.path.join(current_dir, primitive_json_path + "/pipelines", str(json_id) + ".json"), 'w') as fw:
        fw.write(json.dumps(pipeline_dict, indent=2))

    with open(os.path.join(current_dir, primitive_json_path + "/pipelines", str(json_id) + ".meta"), 'w') as fw:
        fw.write(json.dumps(metadata_json, indent=2))

    return


if __name__ == '__main__':
    loop_through()
