from d3m.metadata.pipeline import Pipeline, PrimitiveStep
import d3m.primitives.data_transformation.dataset_to_dataframe
import d3m.primitives.data_transformation.column_parser
import d3m.primitives.data_transformation.construct_predictions
import d3m.primitives.data_transformation.extract_columns_by_semantic_types
import d3m.primitives.data_cleaning.imputer
import d3m.primitives.data_transformation.denormalize
import d3m.primitives.data_preprocessing.text_reader
import d3m.primitives.data_cleaning.string_imputer

from d3m.metadata.base import ArgumentType, Context
import importlib


# Creating pipeline
def pipeline_structure(name_space, store_digest=False) -> (dict, dict):
    import_namespace = ".".join(name_space.split(".")[:-1])
    name_space_class = importlib.import_module(import_namespace)

    if "classification" in name_space or "mutual_info_classif" in name_space:
        outp = generate_classif_pipeline(name_space, name_space_class)
        meta_json = {
            "problem": "185_baseball_problem",
            "full_inputs": ["185_baseball_dataset"],
            "train_inputs": ["185_baseball_dataset_TRAIN"],
            "test_inputs": ["185_baseball_dataset_TEST"],
            "score_inputs": ["185_baseball_dataset_SCORE"]
        }
    elif "regression" in name_space:
        outp = generate_regressor_pipeline(name_space, name_space_class)
        meta_json = {
            "problem": "26_radon_seed_problem",
            "full_inputs": ["26_radon_seed_dataset"],
            "train_inputs": ["26_radon_seed_dataset_TRAIN"],
            "test_inputs": ["26_radon_seed_dataset_TEST"],
            "score_inputs": ["26_radon_seed_dataset_SCORE"]
        }
    elif "vectorizer" in name_space:
        outp = generate_vectorizer_pipeline(name_space, name_space_class)
        meta_json = {
            "problem": "30_personae_problem",
            "full_inputs": ["30_personae_dataset"],
            "train_inputs": ["30_personae_dataset_TRAIN"],
            "test_inputs": ["30_personae_dataset_TEST"],
            "score_inputs": ["30_personae_dataset_SCORE"]
        }
    elif "string_imputer" in name_space:
        outp = generate_str_imputer_pipeline(name_space, name_space_class)
        meta_json = {
            "problem": "LL1_726_TIDY_GPS_carpool_bus_service_rating_prediction_problem",
            "full_inputs": ["LL1_726_TIDY_GPS_carpool_bus_service_rating_prediction_dataset"],
            "train_inputs": ["LL1_726_TIDY_GPS_carpool_bus_service_rating_prediction_dataset_TRAIN"],
            "test_inputs": ["LL1_726_TIDY_GPS_carpool_bus_service_rating_prediction_dataset_TEST"],
            "score_inputs": ["LL1_726_TIDY_GPS_carpool_bus_service_rating_prediction_dataset_SCORE"]
        }
    else:
        outp = generate_transformer_pipeline(name_space, name_space_class)
        meta_json = {
            "problem": "185_baseball_problem",
            "full_inputs": ["185_baseball_dataset"],
            "train_inputs": ["185_baseball_dataset_TRAIN"],
            "test_inputs": ["185_baseball_dataset_TEST"],
            "score_inputs": ["185_baseball_dataset_SCORE"]
        }
    if not store_digest:
        remove_primitive_digest_from_pipeline(outp)
    return outp, meta_json


def remove_primitive_digest_from_pipeline(pipeline:dict):
    del pipeline["digest"]
    for step in pipeline.get("steps", []):
        primitive = step.get("primitive", {})
        del primitive["digest"]


def generate_classif_pipeline(name_space, name_space_class) -> dict:

    pipeline_description = Pipeline(context=Context.TESTING)
    pipeline_description.add_input(name='inputs')

    # # Step 1: DatasetToDataFrame
    step_0 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
    step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
    step_0.add_output('produce')
    pipeline_description.add_step(step_0)

    # Step 2: ColumnParser
    step_1 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.column_parser.Common.metadata.query())
    step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_1.add_output('produce')
    pipeline_description.add_step(step_1)

    # Step 3: imputer
    step_2 = PrimitiveStep(primitive_description=d3m.primitives.data_cleaning.imputer.SKlearn.metadata.query())
    step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
    step_2.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                              data=True)
    step_2.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
                              data='replace')
    step_2.add_output('produce')
    pipeline_description.add_step(step_2)

    # Step 4: Primitive
    step_3 = PrimitiveStep(primitive_description=name_space_class.SKlearn.metadata.query())
    step_3.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                              data=True)
    step_3.add_hyperparameter(name='add_index_columns', argument_type=ArgumentType.VALUE,
                              data=True)
    step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')

    if 'classification' in name_space:
        step_3.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')

    if 'mutual_info_classif' in name_space:
        step_3.add_argument(name='second_inputs', argument_type=ArgumentType.CONTAINER,data_reference='steps.2.produce')

    step_3.add_output('produce')

    list_of_methods = ['produce_cluster_centers', 'produce_feature_importances', 'produce_support']
    for method in list_of_methods:
        produce_method = hasattr(name_space_class.SKlearn, method)
        if produce_method:
            step_3.add_output(method)

    pipeline_description.add_step(step_3)

    # Step 5: ConstructPredictions
    step_4 = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.construct_predictions.Common.metadata.query())
    step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.3.produce')
    step_4.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_4.add_output('produce')
    pipeline_description.add_step(step_4)

    # Final Output
    pipeline_description.add_output(name='output predictions', data_reference='steps.4.produce')

    return pipeline_description.to_json_structure()


def generate_regressor_pipeline(name_space, name_space_class) -> dict:

    pipeline_description = Pipeline(context=Context.TESTING)
    pipeline_description.add_input(name='inputs')

    # # Step 1: DatasetToDataFrame
    step_0 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
    step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
    step_0.add_output('produce')
    pipeline_description.add_step(step_0)

    # Step 2: ColumnParser
    step_1 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.column_parser.Common.metadata.query())
    step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_1.add_output('produce')
    pipeline_description.add_step(step_1)

    # Step 3: Primitive
    step_2 = PrimitiveStep(primitive_description=name_space_class.SKlearn.metadata.query())
    step_2.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                              data=True)
    step_2.add_hyperparameter(name='add_index_columns', argument_type=ArgumentType.VALUE,
                              data=True)
    step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
    step_2.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
    step_2.add_output('produce')

    list_of_methods = ['produce_cluster_centers', 'produce_feature_importances', 'produce_support']
    for method in list_of_methods:
        produce_method = hasattr(name_space_class.SKlearn, method)
        if produce_method:
            step_2.add_output(method)

    pipeline_description.add_step(step_2)

    # Step 4: ConstructPredictions
    step_3 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.construct_predictions.Common.metadata.query())
    step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
    step_3.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_3.add_output('produce')
    pipeline_description.add_step(step_3)

    # Final Output
    pipeline_description.add_output(name='output predictions', data_reference='steps.3.produce')

    return pipeline_description.to_json_structure()


def generate_transformer_pipeline(name_space, name_space_class) -> dict :
    step_num = 0
    pipeline_description = Pipeline(context=Context.TESTING)
    pipeline_description.add_input(name='inputs')

    # Step 0: DatasetToDataFrame
    step_0 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
    step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
    step_0.add_output('produce')
    pipeline_description.add_step(step_0)
    step_num += 1

    # Step 1: ColumnParser
    step_1 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.column_parser.Common.metadata.query())
    step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_1.add_output('produce')
    pipeline_description.add_step(step_1)
    step_num += 1

    # Step 2: ExtractAttributes
    step_2 = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.extract_columns_by_semantic_types.Common.metadata.query())
    step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
    step_2.add_output('produce')
    step_2.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                              data=['https://metadata.datadrivendiscovery.org/types/Attribute'])
    pipeline_description.add_step(step_2)
    step_num += 1

    # Step 3: ExtractTargets
    step_3 = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.extract_columns_by_semantic_types.Common.metadata.query())
    step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_3.add_output('produce')
    step_3.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                              data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
    pipeline_description.add_step(step_3)
    step_num += 1

    # Step 4: Imputer
    attributes = 'steps.2.produce'
    if "missing_indicator" not in name_space:
        step = PrimitiveStep(primitive_description=d3m.primitives.data_cleaning.imputer.SKlearn.metadata.query())
        step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
        step.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                                data=True)
        step.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
                                data='replace')
        step.add_output('produce')
        pipeline_description.add_step(step)
        step_num += 1

        attributes = 'steps.4.produce'

    targets = 'steps.3.produce'

    # Step 5: Transformer
    step = PrimitiveStep(primitive_description=name_space_class.SKlearn.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)
    step.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                            data=True)
    if "random_projection" in name_space:
        step.add_hyperparameter(name='n_components', argument_type=ArgumentType.VALUE,
                                data=2)
    return_replaced = ['one_hot_encoder', 'ordinal_encoder']
    # For baseball dataset ignore "Player Name" column
    if any(word in name_space for word in return_replaced):
        step.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
                                data="replace")
        step.add_hyperparameter(name='exclude_columns', argument_type=ArgumentType.VALUE,
                                data=[0])

    require_output_step = ['variance_threshold','rfe', 'select_fwe', 'generic_univariate_select', 'select_percentile']
    if any(word in name_space for word in require_output_step):
        step.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
        step.add_hyperparameter(name="exclude_inputs_columns", argument_type=ArgumentType.VALUE,
                                data=[0, 16])

    step.add_output('produce')

    list_of_methods = ['produce_cluster_centers', 'produce_feature_importances', 'produce_support']
    for method in list_of_methods:
        produce_method = hasattr(name_space_class.SKlearn, method)
        if produce_method:
            step.add_output(method)

    pipeline_description.add_step(step)
    step_num += 1

    # Step 6: SKRandomForestClassifier
    step_5 = PrimitiveStep(primitive_description=d3m.primitives.classification.random_forest.SKlearn.metadata.query())
    step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.{}.produce'.format(step_num-1))
    step_5.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.3.produce')
    step_5.add_output('produce')
    pipeline_description.add_step(step_5)
    step_num += 1

    # Step 7: ConstructPredictions
    step_6 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.construct_predictions.Common.metadata.query())
    step_6.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.{}.produce'.format(step_num-1))
    step_6.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_6.add_output('produce')
    pipeline_description.add_step(step_6)
    step_num += 1

    # Final Output
    pipeline_description.add_output(name='output predictions', data_reference='steps.{}.produce'.format(step_num-1))

    return pipeline_description.to_json_structure()


def generate_vectorizer_pipeline(name_space, name_space_class) -> dict:
    pipeline_description = Pipeline(context=Context.TESTING)
    pipeline_description.add_input(name='inputs')
    step_counter = 0

    # Step 0: Denormalize
    step = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.denormalize.Common.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
    step.add_output('produce')
    pipeline_description.add_step(step)
    denormalize_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Step: DatasetToDataframe
    step = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=denormalize_output)
    step.add_output('produce')
    pipeline_description.add_step(step)
    dataset_to_dataframe_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Step: ColumnParser
    step = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.column_parser.Common.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=dataset_to_dataframe_output)
    step.add_output('produce')
    pipeline_description.add_step(step)
    column_parser_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Step Imputer:
    step = PrimitiveStep(primitive_description=d3m.primitives.data_cleaning.imputer.SKlearn.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=column_parser_output)
    step.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                            data=True)
    step.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
                            data='replace')
    step.add_output('produce')
    pipeline_description.add_step(step)
    imputer_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Step: Text reader
    step = PrimitiveStep(
        primitive_description=d3m.primitives.data_preprocessing.text_reader.Common.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=imputer_output)
    step.add_output('produce')
    step.add_hyperparameter(name="return_result", argument_type=ArgumentType.VALUE, data="replace")
    pipeline_description.add_step(step)
    text_reader_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Step: Vectorizer
    step = PrimitiveStep(primitive_description=name_space_class.SKlearn.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=text_reader_output)
    step.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                            data=True)
    step.add_hyperparameter(name="return_result", argument_type=ArgumentType.VALUE, data="replace")
    step.add_output('produce')

    list_of_methods = ['produce_cluster_centers', 'produce_feature_importances', 'produce_support']
    for method in list_of_methods:
        produce_method = hasattr(name_space_class.SKlearn, method)
        if produce_method:
            step.add_output(method)

    pipeline_description.add_step(step)
    vectorizer_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Step: Learner
    step = PrimitiveStep(primitive_description=d3m.primitives.classification.random_forest.SKlearn.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=vectorizer_output)
    step.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=vectorizer_output)
    step.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                            data=True)
    step.add_output('produce')
    pipeline_description.add_step(step)
    learner_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Step 7: Construct Predictions
    step = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.construct_predictions.Common.metadata.query())
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=learner_output)
    step.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference=dataset_to_dataframe_output)
    step.add_output('produce')
    pipeline_description.add_step(step)
    construct_predictions_output = "steps.{}.produce".format(step_counter)
    step_counter += 1

    # Final Output
    pipeline_description.add_output(name='output predictions', data_reference=construct_predictions_output)

    return pipeline_description.to_json_structure()

def generate_str_imputer_pipeline(name_space, name_space_class) -> dict:
    pipeline_description = Pipeline(context=Context.TESTING)
    pipeline_description.add_input(name='inputs')
    # # Step 1: DatasetToDataFrame
    step_0 = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.dataset_to_dataframe.Common.metadata.query())
    step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
    step_0.add_output('produce')
    pipeline_description.add_step(step_0)

    # Step 3: imputer
    step_3 = PrimitiveStep(primitive_description=d3m.primitives.data_cleaning.string_imputer.SKlearn.metadata.query())
    step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_3.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                              data=True)
    step_3.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
                              data='replace')
    step_3.add_output('produce')
    pipeline_description.add_step(step_3)

    # Step 2: ColumnParser
    step_1 = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.column_parser.Common.metadata.query())
    step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
    step_1.add_output('produce')
    pipeline_description.add_step(step_1)

    # Step 3: ExtractTargets
    step_2 = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.extract_columns_by_semantic_types.Common.metadata.query())
    step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_2.add_output('produce')
    step_2.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                              data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
    pipeline_description.add_step(step_2)

    # # Step 3: imputer
    # step_3 = PrimitiveStep(primitive_description=d3m.primitives.data_cleaning.string_imputer.SKlearn.metadata.query())
    # step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
    # step_3.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
    #                           data=True)
    # step_3.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE,
    #                           data='replace')
    # step_3.add_output('produce')
    # pipeline_description.add_step(step_3)

    # Step 4: Primitive
    step_4 = PrimitiveStep(primitive_description=d3m.primitives.classification.random_forest.SKlearn.metadata.query())
    step_4.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                              data=True)
    step_4.add_hyperparameter(name='add_index_columns', argument_type=ArgumentType.VALUE,
                              data=True)
    step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
    step_4.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.3.produce')
    step_4.add_output('produce')
    pipeline_description.add_step(step_4)

    # Step 5: ConstructPredictions
    step_4 = PrimitiveStep(
        primitive_description=d3m.primitives.data_transformation.construct_predictions.Common.metadata.query())
    step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.4.produce')
    step_4.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_4.add_output('produce')
    pipeline_description.add_step(step_4)

    # Final Output
    pipeline_description.add_output(name='output predictions', data_reference='steps.4.produce')
    return pipeline_description.to_json_structure()


