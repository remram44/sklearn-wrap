{
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "K Neighbors Classifier",
  "id": "79a6b9b7-b0d1-36c7-9833-7cc5de739a65",
  "category": "neighbors.classification",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/neighbors/classification.py#L23",
  "parameters": [
    {
      "type": "int",
      "optional": "true",
      "name": "n_neighbors",
      "description": "Number of neighbors to use by default for :meth:`k_neighbors` queries. "
    },
    {
      "type": "str",
      "optional": "true",
      "name": "weights",
      "description": "weight function used in prediction.  Possible values:  - 'uniform' : uniform weights.  All points in each neighborhood are weighted equally. - 'distance' : weight points by the inverse of their distance. in this case, closer neighbors of a query point will have a greater influence than neighbors which are further away. - [callable] : a user-defined function which accepts an array of distances, and returns an array of the same shape containing the weights. "
    },
    {
      "type": "'auto', 'ball_tree', 'kd_tree', 'brute'",
      "optional": "true",
      "name": "algorithm",
      "description": "Algorithm used to compute the nearest neighbors:  - 'ball_tree' will use :class:`BallTree` - 'kd_tree' will use :class:`KDTree` - 'brute' will use a brute-force search. - 'auto' will attempt to decide the most appropriate algorithm based on the values passed to :meth:`fit` method.  Note: fitting on sparse input will override the setting of this parameter, using brute force. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "leaf_size",
      "description": "Leaf size passed to BallTree or KDTree.  This can affect the speed of the construction and query, as well as the memory required to store the tree.  The optimal value depends on the nature of the problem. "
    },
    {
      "type": "string",
      "name": "metric",
      "description": "the distance metric to use for the tree.  The default metric is minkowski, and with p=2 is equivalent to the standard Euclidean metric. See the documentation of the DistanceMetric class for a list of available metrics. "
    },
    {
      "type": "integer",
      "optional": "true",
      "name": "p",
      "description": "Power parameter for the Minkowski metric. When p = 1, this is equivalent to using manhattan_distance (l1), and euclidean_distance (l2) for p = 2. For arbitrary p, minkowski_distance (l_p) is used. "
    },
    {
      "type": "dict",
      "optional": "true",
      "name": "metric_params",
      "description": "Additional keyword arguments for the metric function. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "n_jobs",
      "description": "The number of parallel jobs to run for neighbors search. If ``-1``, then the number of jobs is set to the number of CPU cores. Doesn't affect :meth:`fit` method. "
    }
  ],
  "tags": [
    "neighbors",
    "classification"
  ],
  "common_name_unanalyzed": "K Neighbors Classifier",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "description": "\"Classifier implementing the k-nearest neighbors vote.\n\nRead more in the :ref:`User Guide <classification>`.\n",
  "methods_available": [
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.fit",
      "description": "\"Fit the model using X as training data and y as target values\n",
      "parameters": [
        {
          "type": "array-like, sparse matrix, BallTree, KDTree",
          "name": "X",
          "description": "Training data. If array or matrix, shape [n_samples, n_features], or [n_samples, n_samples] if metric='precomputed'. "
        },
        {
          "type": "array-like, sparse matrix",
          "name": "y",
          "description": "Target values of shape = [n_samples] or [n_samples, n_outputs]  \""
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.kneighbors",
      "returns": {
        "type": "array",
        "name": "dist",
        "description": "Array representing the lengths to points, only present if return_distance=True  ind : array Indices of the nearest points in the population matrix.  Examples -------- In the following example, we construct a NeighborsClassifier class from an array representing our data set and ask who's the closest point to [1,1,1]  >>> samples = [[0., 0., 0.], [0., .5, 0.], [1., 1., .5]] >>> from sklearn.neighbors import NearestNeighbors >>> neigh = NearestNeighbors(n_neighbors=1) >>> neigh.fit(samples) # doctest: +ELLIPSIS NearestNeighbors(algorithm='auto', leaf_size=30, ...) >>> print(neigh.kneighbors([[1., 1., 1.]])) # doctest: +ELLIPSIS (array([[ 0.5]]), array([[2]]...))  As you can see, it returns [[0.5]], and [[2]], which means that the element is at distance 0.5 and is the third element of samples (indexes start at 0). You can also query for multiple points:  >>> X = [[0., 1., 0.], [1., 0., 1.]] >>> neigh.kneighbors(X, return_distance=False) # doctest: +ELLIPSIS array([[1], [2]]...)  \""
      },
      "description": "\"Finds the K-neighbors of a point.\n\nReturns indices of and distances to the neighbors of each point.\n",
      "parameters": [
        {
          "shape": "n_query, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The query point or points. If not provided, neighbors of each indexed point are returned. In this case, the query point is not considered its own neighbor. "
        },
        {
          "type": "int",
          "name": "n_neighbors",
          "description": "Number of neighbors to get (default is the value passed to the constructor). "
        },
        {
          "type": "boolean",
          "optional": "true",
          "name": "return_distance",
          "description": "If False, distances will not be returned "
        }
      ],
      "name": "kneighbors"
    },
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.kneighbors_graph",
      "returns": {
        "shape": "n_samples, n_samples_fit",
        "type": "sparse",
        "name": "A",
        "description": "n_samples_fit is the number of samples in the fitted data A[i, j] is assigned the weight of edge that connects i to j.  Examples -------- >>> X = [[0], [3], [1]] >>> from sklearn.neighbors import NearestNeighbors >>> neigh = NearestNeighbors(n_neighbors=2) >>> neigh.fit(X) # doctest: +ELLIPSIS NearestNeighbors(algorithm='auto', leaf_size=30, ...) >>> A = neigh.kneighbors_graph(X) >>> A.toarray() array([[ 1.,  0.,  1.], [ 0.,  1.,  1.], [ 1.,  0.,  1.]])  See also -------- NearestNeighbors.radius_neighbors_graph \""
      },
      "description": "\"Computes the (weighted) graph of k-Neighbors for points in X\n",
      "parameters": [
        {
          "shape": "n_query, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The query point or points. If not provided, neighbors of each indexed point are returned. In this case, the query point is not considered its own neighbor. "
        },
        {
          "type": "int",
          "name": "n_neighbors",
          "description": "Number of neighbors for each sample. (default is value passed to the constructor). "
        },
        {
          "type": "'connectivity', 'distance'",
          "optional": "true",
          "name": "mode",
          "description": "Type of returned matrix: 'connectivity' will return the connectivity matrix with ones and zeros, in 'distance' the edges are Euclidean distance between points. "
        }
      ],
      "name": "kneighbors_graph"
    },
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "y",
        "description": "Class labels for each data sample. \""
      },
      "description": "\"Predict the class labels for the provided data\n",
      "parameters": [
        {
          "shape": "n_query, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.predict_proba",
      "returns": {
        "shape": "n_samples, n_classes",
        "type": "array",
        "name": "p",
        "description": "of such arrays if n_outputs > 1. The class probabilities of the input samples. Classes are ordered by lexicographic order. \""
      },
      "description": "\"Return probability estimates for the test data X.\n",
      "parameters": [
        {
          "shape": "n_query, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        }
      ],
      "name": "predict_proba"
    },
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      },
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.neighbors.classification.KNeighborsClassifier.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    }
  ],
  "algorithm_type": [
    "classification"
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/neighbors/classification.py#L23",
  "category_unanalyzed": "neighbors.classification",
  "name": "sklearn.neighbors.classification.KNeighborsClassifier",
  "team": "jpl",
  "attributes": []
}
