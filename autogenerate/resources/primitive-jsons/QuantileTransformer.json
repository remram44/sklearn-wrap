{
  "name": "sklearn.preprocessing.data.QuantileTransformer",
  "id": "a22109dd5de87595352f2256d585f958",
  "common_name": "QuantileTransformer",
  "is_class": true,
  "tags": [
    "preprocessing",
    "data"
  ],
  "version": "0.20.3",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.20.3/sklearn/preprocessing/data.py#L2002",
  "parameters": [
    {
      "type": "int",
      "optional": "true",
      "default": "1000",
      "name": "n_quantiles",
      "description": "Number of quantiles to be computed. It corresponds to the number of landmarks used to discretize the cumulative distribution function. "
    },
    {
      "type": "str",
      "optional": "true",
      "default": "'uniform'",
      "name": "output_distribution",
      "description": "Marginal distribution for the transformed data. The choices are 'uniform' (default) or 'normal'. "
    },
    {
      "type": "bool",
      "optional": "true",
      "default": "False",
      "name": "ignore_implicit_zeros",
      "description": "Only applies to sparse matrices. If True, the sparse entries of the matrix are discarded to compute the quantile statistics. If False, these entries are treated as zeros. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "1e5",
      "name": "subsample",
      "description": "Maximum number of samples used to estimate the quantiles for computational efficiency. Note that the subsampling procedure may differ for value-identical sparse and dense matrices. "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random. Note that this is used by subsampling and smoothing noise. "
    },
    {
      "type": "boolean",
      "optional": "true",
      "name": "copy",
      "description": "Set to False to perform inplace transformation and avoid a copy (if the input is already a numpy array). "
    }
  ],
  "attributes": [
    {
      "type": "ndarray",
      "shape": "n_quantiles, n_features",
      "name": "quantiles_",
      "description": "The values corresponding the quantiles of reference. "
    },
    {
      "type": "ndarray",
      "shape": "n_quantiles, ",
      "name": "references_",
      "description": "Quantiles of references. "
    }
  ],
  "description": "\"Transform features using quantiles information.\n\nThis method transforms the features to follow a uniform or a normal\ndistribution. Therefore, for a given feature, this transformation tends\nto spread out the most frequent values. It also reduces the impact of\n(marginal) outliers: this is therefore a robust preprocessing scheme.\n\nThe transformation is applied on each feature independently.\nThe cumulative distribution function of a feature is used to project the\noriginal values. Features values of new/unseen data that fall below\nor above the fitted range will be mapped to the bounds of the output\ndistribution. Note that this transform is non-linear. It may distort linear\ncorrelations between variables measured at the same scale but renders\nvariables measured at different scales more directly comparable.\n\nRead more in the :ref:`User Guide <preprocessing_transformer>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.preprocessing.data.QuantileTransformer.fit",
      "parameters": [
        {
          "type": "ndarray",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The data used to scale along the features axis. If a sparse matrix is provided, it will be converted into a sparse ``csc_matrix``. Additionally, the sparse matrix needs to be nonnegative if `ignore_implicit_zeros` is False. "
        }
      ],
      "description": "'Compute the quantiles used for transforming.\n",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "fit_transform",
      "id": "sklearn.preprocessing.data.QuantileTransformer.fit_transform",
      "parameters": [
        {
          "type": "numpy",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training set. "
        },
        {
          "type": "numpy",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "returns": {
        "type": "numpy",
        "shape": "n_samples, n_features_new",
        "name": "X_new",
        "description": "Transformed array.  '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.preprocessing.data.QuantileTransformer.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "inverse_transform",
      "id": "sklearn.preprocessing.data.QuantileTransformer.inverse_transform",
      "parameters": [
        {
          "type": "ndarray",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The data used to scale along the features axis. If a sparse matrix is provided, it will be converted into a sparse ``csc_matrix``. Additionally, the sparse matrix needs to be nonnegative if `ignore_implicit_zeros` is False. "
        }
      ],
      "description": "'Back-projection to the original space.\n",
      "returns": {
        "type": "ndarray",
        "shape": "n_samples, n_features",
        "name": "Xt",
        "description": "The projected data. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.preprocessing.data.QuantileTransformer.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "transform",
      "id": "sklearn.preprocessing.data.QuantileTransformer.transform",
      "parameters": [
        {
          "type": "ndarray",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The data used to scale along the features axis. If a sparse matrix is provided, it will be converted into a sparse ``csc_matrix``. Additionally, the sparse matrix needs to be nonnegative if `ignore_implicit_zeros` is False. "
        }
      ],
      "description": "'Feature-wise transformation of the data.\n",
      "returns": {
        "type": "ndarray",
        "shape": "n_samples, n_features",
        "name": "Xt",
        "description": "The projected data. '"
      }
    }
  ]
}