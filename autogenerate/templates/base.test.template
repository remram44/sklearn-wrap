import unittest
import pickle

from autogenerate.d3m_sklearn_wrap.sklearn_wrap import {{ class_name }}
from tests.params_check import params_check
from d3m.metadata import base as metadata_base
from d3m import container
from d3m.primitive_interfaces.base import PrimitiveBase
from d3m.exceptions import PrimitiveNotFittedError
from pandas.util.testing import assert_frame_equal

import os
from common_primitives import dataset_to_dataframe, column_parser

{% block dataset %}
{%- if test_data == None -%}
{% set test_data = "tests-data/datasets/iris_dataset_1" %}{% endif %}
{% include "test_data.template" %}
{% endblock %}
{% block data_split %}{% endblock %}

semantic_types_to_remove = set({{ remove_semantic_types }})
semantic_types_to_add = set({{ add_semantic_types }})

# We want to test the running of the code without errors and not the correctness of it
# since that is assumed to be tested by sklearn

class Test{{ class_name }}(unittest.TestCase):
    def create_learner(self, hyperparams):
        clf = {{ class_name }}.{{ class_name }}(hyperparams=hyperparams)
        return clf

    def set_training_data_on_learner(self, learner, **args):
        learner.set_training_data(**args)

    def fit_learner(self, learner: PrimitiveBase):
        learner.fit()

    def produce_learner(self, learner, **args):
        return learner.produce(**args)

    def basic_fit(self, hyperparams):
        learner = self.create_learner(hyperparams)
        training_data_args = self.set_data(hyperparams)
        self.set_training_data_on_learner(learner, **training_data_args)

        self.assertRaises(PrimitiveNotFittedError, learner.produce, inputs=training_data_args.get("inputs"))

        self.fit_learner(learner)

        assert len(learner._training_indices) > 0

        output = self.produce_learner(learner, inputs=training_data_args.get("inputs"))
        return output, learner, training_data_args

    def pickle(self, hyperparams):
        output, learner, training_data_args = self.basic_fit(hyperparams)

        # Testing get_params() and set_params()
        params = learner.get_params()
        learner.set_params(params=params)

        model = pickle.dumps(learner)
        new_clf = pickle.loads(model)
        new_output = new_clf.produce(inputs=training_data_args.get("inputs"))

        assert_frame_equal(new_output.value, output.value)

    {% block set_arguments %}
    def set_data(self, hyperparams):
        hyperparams = hyperparams.get("use_semantic_types")
        if hyperparams:
            return {"inputs": train_set, "outputs": targets}
        else:
            return {"inputs": parsed_dataframe.select_columns([1, 2, 3, 4]), "outputs": parsed_dataframe.select_columns([5])}
    {% endblock -%}
    {% block get_transformed_indices %}
    def get_transformed_indices(self, learner):
        return learner._target_column_indices
    {% endblock %}
    {% block new_return_checker %}
    def new_return_checker(self, output, indices):
        input_target = train_set.select_columns(list(indices))
        for i in range(len(output.columns)):
            input_semantic_types = input_target.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types")
            output_semantic_type = set(
                output.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            transformed_input_semantic_types = set(input_semantic_types) - semantic_types_to_remove
            transformed_input_semantic_types = transformed_input_semantic_types.union(semantic_types_to_add)
            assert output_semantic_type == transformed_input_semantic_types
    {% endblock %}
    {% block append_return_checker %}
    def append_return_checker(self, output, indices):
        for i in range(len(train_set.columns)):
            input_semantic_types = set(train_set.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            assert output_semantic_type == input_semantic_types

        self.new_return_checker(
            output.value.select_columns(list(range(len(train_set.columns), len(output.value.columns)))),
            indices)
    {% endblock %}
    {% block replace_return_checker %}
    def replace_return_checker(self, output, indices):
        for i in range(len(train_set.columns)):
            if i in indices:
                continue
            input_semantic_types = set(train_set.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            assert output_semantic_type == input_semantic_types

        self.new_return_checker(output.value.select_columns(list(indices)), indices)
    {% endblock %}
    def test_with_semantic_types(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults().replace({"use_semantic_types": True})
        self.pickle(hyperparams)

    def test_without_semantic_types(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults()
        self.pickle(hyperparams)

    def test_with_new_return_result(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults().replace({"return_result": 'new', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.new_return_checker(output.value, indices)
    {% block test_with_append_return_result %}
    def test_with_append_return_result(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults().replace({"return_result": 'append', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.append_return_checker(output, indices)
    {% endblock %}
    def test_with_replace_return_result(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults().replace({"return_result": 'replace', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.replace_return_checker(output, indices)

    def test_produce_methods(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults()
        output, clf, _ = self.basic_fit(hyperparams)
        list_of_methods = ['produce_cluster_centers', 'produce_feature_importances','produce_support' ]
        for method in list_of_methods:
            produce_method = getattr(clf, method, None)
            if produce_method:
                produce_method()

    {% block get_target_column_name %}{% endblock %}
    {% block test_params %}def test_params(self):
        params_check({{ class_name }}.{{ class_name }}, train_set)
    {% endblock %}

if __name__ == '__main__':
    unittest.main()
