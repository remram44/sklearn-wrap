{% extends "base.pipeline.template" %}

{% block import %}import d3m.primitives.classification.random_forest
import d3m.primitives.data_transformation.extract_columns_by_semantic_types
{% endblock %}

{% block primitive %}
# Step 3: ExtractAttributes
step_2 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.extract_columns_by_semantic_types.DataFrameCommon.metadata.query())
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_output('produce')
step_2.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                          data=['https://metadata.datadrivendiscovery.org/types/Attribute'])
pipeline_description.add_step(step_2)

# Step 4: ExtractTargets
step_3 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.extract_columns_by_semantic_types.DataFrameCommon.metadata.query())
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_3.add_output('produce')
step_3.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                          data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
pipeline_description.add_step(step_3)

attributes = 'steps.2.produce'
targets = 'steps.3.produce'

# Step 5: Transformer
step_4 = PrimitiveStep(primitive_description={{ class_namespace.strip('\"') }}.metadata.query())
step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)
{% block outputs %}{% endblock %}
step_4.add_output('produce')
pipeline_description.add_step(step_4)

# Step 6: SKRandomForestClassifier
step_5 = PrimitiveStep(primitive_description=d3m.primitives.classification.random_forest.SKlearn.metadata.query())
step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.4.produce')
step_5.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.3.produce')
step_5.add_output('produce')
pipeline_description.add_step(step_5)

# Step 7: ConstructPredictions
step_6 = PrimitiveStep(primitive_description=d3m.primitives.data_transformation.construct_predictions.DataFrameCommon.metadata.query())
step_6.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.5.produce')
step_6.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_6.add_output('produce')
pipeline_description.add_step(step_6)

# Final Output
pipeline_description.add_output(name='output predictions', data_reference='steps.6.produce')
{% endblock %}