{% extends "base.template" %}
{% block interface %}from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase
from d3m.primitive_interfaces.base import singleton {% endblock %}

{% block classname %}class SK{{ class_name }}(TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):{% endblock %}
{% block produce %}{% if flags and "singleton" in flags %}
    @singleton{% endif %}
    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        clf = {{ class_name }}({% for h in hyperparams %}
            {{ h['name'] }}={% if h['eval'] %}eval(self.hyperparams['{{ h['name'] }}']){% else %}self.hyperparams['{{ h['name'] }}']{% endif %},{% endfor %}{% for arg in constructor_args if not arg['name'] == 'verbose'%}
            {% if arg['name'] == 'random_state' %}random_state=self.random_seed,{% else %}{{ arg['name'] }}=_{{ arg['name'] }}{% if not loop.last %},{% endif %}{% endif %}{% endfor %}
        )
        sk_inputs = inputs
        training_indices = []
        if self.hyperparams['use_semantic_types']:
            sk_inputs, training_indices = self._get_columns_to_fit(inputs, self.hyperparams)
        output = clf.fit_transform(sk_inputs)
        if sparse.issparse(output):
            output = pandas.DataFrame.sparse.from_spmatrix(output)
        output = d3m_dataframe(output, generate_metadata=False)
        output.metadata = inputs.metadata.clear(for_value=output, generate_metadata=True)
        output.metadata = self._add_target_semantic_types(metadata=output.metadata, source=self)
        outputs = base_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=self.hyperparams['add_index_columns'],
                                               inputs=inputs, column_indices=training_indices,
                                               columns_list=[output])
        return CallResult(outputs)
        {% endblock %}

