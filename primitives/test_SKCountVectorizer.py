import unittest
import numpy as np
import pickle
from typing import NamedTuple

from sklearn import datasets
from sklearn.utils import shuffle
import scipy.sparse
from sklearn.feature_extraction.text import CountVectorizer
from autogenerate.d3m_sklearn_wrap.sklearn_wrap import SKCountVectorizer
from d3m import container
from pandas.util.testing import assert_frame_equal
from sklearn.datasets import fetch_20newsgroups
fetch_20newsgroups()
categories = ['alt.atheism', 'soc.religion.christian','comp.graphics', 'sci.med']
twenty_train = fetch_20newsgroups(subset='train', categories=categories, shuffle=True, random_state=42)


# Common random state
rng = np.random.RandomState(0)

# Load the iris dataset and randomly permute it
iris = datasets.load_iris()
perm = rng.permutation(iris.target.size)
iris.data, iris.target = shuffle(iris.data, iris.target, random_state=rng)


class TestSKCountVectorizer(unittest.TestCase):
    def test_fit_iris(self):
        classes = np.unique(iris.target)
        hyperparams = SKCountVectorizer.Hyperparams.defaults()


        skclf = CountVectorizer()
        clf = SKCountVectorizer.SKCountVectorizer(hyperparams=hyperparams)

        train_set = container.DataFrame(twenty_train.data)
        print(type(train_set.iloc[0]))
        clf.set_training_data(inputs=train_set)
        clf.fit()



        output = clf.produce(inputs=train_set)
        print(output.value)
        first_output = clf.produce(inputs=train_set)
        new_clf = SKCountVectorizer.SKCountVectorizer(hyperparams=hyperparams)
        new_clf.set_training_data(inputs=train_set)
        new_clf.fit()



        # assert_frame_equal(first_output.value, output.value)
        # assert_frame_equal(new_output.value, output.value)
        # We want to test the running of the code without errors and not the correctness of it
        # since that is assumed to be tested by sklearn
