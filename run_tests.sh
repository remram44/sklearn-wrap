#!/bin/bash

PARALLEL=1

if [ ! -z "$1" ]; then
	PARALLEL=$1
fi

time ls tests/*.py | xargs -n 1 -Ifile echo file | sed 's/\//./' | sed 's/\.py//' | xargs -n 1 -P $PARALLEL -Ifile python3 -m file
