#!/bin/bash

PARALLEL=1

if [ ! -z "$1" ]; then
	PARALLEL=$1
fi

time ls *.py | xargs -n 1 -P $PARALLEL -Ifile python3 file
time ls *.json | xargs -n 1 -Ifile -P $PARALLEL python3 ../../autogenerate/run_pipeline.py file